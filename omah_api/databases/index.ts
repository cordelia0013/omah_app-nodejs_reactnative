import { PrismaClient } from '@prisma/client'
import dotenv from 'dotenv'
import path from 'path'

const envPath = path.join(__dirname, '../../');
dotenv.config({ path: envPath + '.env' })

const prisma = new PrismaClient({log: ['query'] })

export default prisma