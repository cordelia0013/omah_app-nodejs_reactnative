/*
  Warnings:

  - You are about to drop the column `tel` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `username` on the `User` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `User` DROP COLUMN `tel`,
    DROP COLUMN `username`,
    ADD COLUMN `role` ENUM('DEFAULT', 'ADMIN', 'CHEF') NOT NULL DEFAULT 'DEFAULT',
    MODIFY `firstname` VARCHAR(191) NULL,
    MODIFY `lastname` VARCHAR(191) NULL,
    MODIFY `email` VARCHAR(191) NULL,
    MODIFY `password` VARCHAR(100) NULL;

-- CreateTable
CREATE TABLE `Menu` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `tite_menu` VARCHAR(50) NOT NULL,
    `description` VARCHAR(150) NOT NULL,
    `composition` VARCHAR(100) NOT NULL,
    `prix` DOUBLE NOT NULL,
    `qte_part` INTEGER NULL,
    `statue_menu` BOOLEAN NOT NULL DEFAULT false,
    `image` VARCHAR(254) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3) NOT NULL,
    `userId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `Menu` ADD CONSTRAINT `Menu_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
