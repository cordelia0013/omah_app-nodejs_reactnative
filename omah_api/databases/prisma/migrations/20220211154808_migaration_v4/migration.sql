/*
  Warnings:

  - You are about to drop the column `montant` on the `LigneCmd` table. All the data in the column will be lost.
  - You are about to drop the `Avis` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Facture` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[billId]` on the table `LigneCmd` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `amount` to the `LigneCmd` table without a default value. This is not possible if the table is not empty.
  - Added the required column `billId` to the `LigneCmd` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE `Avis` DROP FOREIGN KEY `Avis_menuId_fkey`;

-- DropForeignKey
ALTER TABLE `Avis` DROP FOREIGN KEY `Avis_userId_fkey`;

-- DropForeignKey
ALTER TABLE `Facture` DROP FOREIGN KEY `Facture_orderId_fkey`;

-- AlterTable
ALTER TABLE `LigneCmd` DROP COLUMN `montant`,
    ADD COLUMN `amount` DOUBLE NOT NULL,
    ADD COLUMN `billId` INTEGER NOT NULL;

-- DropTable
DROP TABLE `Avis`;

-- DropTable
DROP TABLE `Facture`;

-- CreateTable
CREATE TABLE `Advice` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `commentaire` VARCHAR(254) NOT NULL,
    `userId` INTEGER NOT NULL,
    `menuId` INTEGER NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    UNIQUE INDEX `Advice_menuId_userId_key`(`menuId`, `userId`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Bill` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `date_facture` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `orderId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `LigneCmd_billId_key` ON `LigneCmd`(`billId`);

-- AddForeignKey
ALTER TABLE `Advice` ADD CONSTRAINT `Advice_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Advice` ADD CONSTRAINT `Advice_menuId_fkey` FOREIGN KEY (`menuId`) REFERENCES `Menu`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Bill` ADD CONSTRAINT `Bill_orderId_fkey` FOREIGN KEY (`orderId`) REFERENCES `Order`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `LigneCmd` ADD CONSTRAINT `LigneCmd_billId_fkey` FOREIGN KEY (`billId`) REFERENCES `Bill`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
