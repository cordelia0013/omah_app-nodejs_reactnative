/*
  Warnings:

  - You are about to drop the column `uid` on the `User` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX `users_uid_uindex` ON `User`;

-- AlterTable
ALTER TABLE `User` DROP COLUMN `uid`;

-- RedefineIndex
CREATE UNIQUE INDEX `Adresse_userId_unique` ON `Adresse`(`userId`);
DROP INDEX `Adresse_userId_key` ON `Adresse`;

-- RedefineIndex
CREATE UNIQUE INDEX `Advice.menuId_userId_unique` ON `Advice`(`menuId`, `userId`);
DROP INDEX `Advice_menuId_userId_key` ON `Advice`;

-- RedefineIndex
CREATE UNIQUE INDEX `Favory.menuId_userId_unique` ON `Favory`(`menuId`, `userId`);
DROP INDEX `Favory_menuId_userId_key` ON `Favory`;

-- RedefineIndex
CREATE UNIQUE INDEX `LigneCmd_billId_unique` ON `LigneCmd`(`billId`);
DROP INDEX `LigneCmd_billId_key` ON `LigneCmd`;

-- RedefineIndex
CREATE UNIQUE INDEX `User.email_unique` ON `User`(`email`);
DROP INDEX `User_email_key` ON `User`;

-- RedefineIndex
CREATE UNIQUE INDEX `User.email_token_unique` ON `User`(`email_token`);
DROP INDEX `User_email_token_key` ON `User`;

-- RedefineIndex
CREATE UNIQUE INDEX `receivMessage_receiverId_unique` ON `receivMessage`(`receiverId`);
DROP INDEX `receivMessage_receiverId_key` ON `receivMessage`;

-- RedefineIndex
CREATE UNIQUE INDEX `sendMessage_senderId_unique` ON `sendMessage`(`senderId`);
DROP INDEX `sendMessage_senderId_key` ON `sendMessage`;
