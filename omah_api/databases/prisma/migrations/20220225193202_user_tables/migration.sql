/*
  Warnings:

  - You are about to drop the column `email_token` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `password_token` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `role` on the `User` table. All the data in the column will be lost.
  - You are about to drop the `Adresse` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Advice` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Bill` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Favory` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `LigneCmd` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Loyalty_point` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Menu` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Message` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Notification` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Order` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `receivMessage` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `sendMessage` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `username` to the `User` table without a default value. This is not possible if the table is not empty.
  - Made the column `tel` on table `User` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE `Adresse` DROP FOREIGN KEY `Adresse_userId_fkey`;

-- DropForeignKey
ALTER TABLE `Advice` DROP FOREIGN KEY `Advice_menuId_fkey`;

-- DropForeignKey
ALTER TABLE `Advice` DROP FOREIGN KEY `Advice_userId_fkey`;

-- DropForeignKey
ALTER TABLE `Bill` DROP FOREIGN KEY `Bill_orderId_fkey`;

-- DropForeignKey
ALTER TABLE `Favory` DROP FOREIGN KEY `Favory_menuId_fkey`;

-- DropForeignKey
ALTER TABLE `Favory` DROP FOREIGN KEY `Favory_userId_fkey`;

-- DropForeignKey
ALTER TABLE `LigneCmd` DROP FOREIGN KEY `LigneCmd_billId_fkey`;

-- DropForeignKey
ALTER TABLE `LigneCmd` DROP FOREIGN KEY `LigneCmd_menuId_fkey`;

-- DropForeignKey
ALTER TABLE `LigneCmd` DROP FOREIGN KEY `LigneCmd_orderId_fkey`;

-- DropForeignKey
ALTER TABLE `Loyalty_point` DROP FOREIGN KEY `Loyalty_point_userId_fkey`;

-- DropForeignKey
ALTER TABLE `Message` DROP FOREIGN KEY `Message_messageId_fkey`;

-- DropForeignKey
ALTER TABLE `Message` DROP FOREIGN KEY `Message_notificationId_fkey`;

-- DropForeignKey
ALTER TABLE `Notification` DROP FOREIGN KEY `Notification_userId_fkey`;

-- DropForeignKey
ALTER TABLE `receivMessage` DROP FOREIGN KEY `receivMessage_receiverId_fkey`;

-- DropForeignKey
ALTER TABLE `sendMessage` DROP FOREIGN KEY `sendMessage_senderId_fkey`;

-- DropIndex
DROP INDEX `User.email_token_unique` ON `User`;

-- AlterTable
ALTER TABLE `User` DROP COLUMN `email_token`,
    DROP COLUMN `password_token`,
    DROP COLUMN `role`,
    ADD COLUMN `username` VARCHAR(50) NOT NULL,
    MODIFY `tel` INTEGER NOT NULL;

-- DropTable
DROP TABLE `Adresse`;

-- DropTable
DROP TABLE `Advice`;

-- DropTable
DROP TABLE `Bill`;

-- DropTable
DROP TABLE `Favory`;

-- DropTable
DROP TABLE `LigneCmd`;

-- DropTable
DROP TABLE `Loyalty_point`;

-- DropTable
DROP TABLE `Menu`;

-- DropTable
DROP TABLE `Message`;

-- DropTable
DROP TABLE `Notification`;

-- DropTable
DROP TABLE `Order`;

-- DropTable
DROP TABLE `receivMessage`;

-- DropTable
DROP TABLE `sendMessage`;

-- RedefineIndex
CREATE UNIQUE INDEX `User_email_key` ON `User`(`email`);
DROP INDEX `User.email_unique` ON `User`;
