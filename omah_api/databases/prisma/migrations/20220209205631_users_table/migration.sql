-- CreateTable
CREATE TABLE `users` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `uid` VARCHAR(100) NOT NULL,
    `firstname` VARCHAR(50) NOT NULL,
    `lastname` VARCHAR(50) NOT NULL,
    `birthdate` DATE NULL,
    `email` VARCHAR(100) NOT NULL,
    `tel` INTEGER NULL,
    `city` VARCHAR(50) NOT NULL,
    `post_code` INTEGER NOT NULL,
    `student` BOOLEAN NOT NULL DEFAULT false,
    `password` VARCHAR(100) NOT NULL,
    `password_token` VARCHAR(255) NULL,
    `email_token` VARCHAR(255) NULL,
    `is_email_verified` BOOLEAN NOT NULL DEFAULT false,

    UNIQUE INDEX `users_uid_uindex`(`uid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
