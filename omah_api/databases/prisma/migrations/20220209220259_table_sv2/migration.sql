/*
  Warnings:

  - You are about to drop the `users` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE `users`;

-- CreateTable
CREATE TABLE `Users` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `uid` VARCHAR(100) NOT NULL,
    `firstname` VARCHAR(50) NULL,
    `lastname` VARCHAR(50) NULL,
    `tel` INTEGER NULL,
    `email` VARCHAR(100) NOT NULL,
    `email_token` VARCHAR(255) NULL,
    `password` VARCHAR(100) NOT NULL,
    `password_token` VARCHAR(255) NULL,
    `dateCreaCompte` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `is_email_verified` BOOLEAN NOT NULL DEFAULT false,

    UNIQUE INDEX `users_uid_uindex`(`uid`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Adresse` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `adresse` VARCHAR(100) NOT NULL,
    `cp` INTEGER NOT NULL,
    `Ville` VARCHAR(100) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Loyalty_point` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `firstname` VARCHAR(50) NOT NULL,
    `point` INTEGER NOT NULL,
    `date_Point` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Login` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `user_name` VARCHAR(150) NOT NULL,
    `role` ENUM('USERS', 'ADMIN', 'CHEF') NOT NULL DEFAULT 'USERS',
    `usersId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Menu` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `tite_menu` VARCHAR(50) NOT NULL,
    `description` VARCHAR(150) NOT NULL,
    `composition` VARCHAR(100) NOT NULL,
    `prix` DOUBLE NOT NULL,
    `qte_part` INTEGER NULL,
    `statue_menu` BOOLEAN NOT NULL DEFAULT false,
    `image` VARCHAR(254) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `favory` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `firstname_users` VARCHAR(100) NOT NULL,
    `tite_menu` VARCHAR(100) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Order` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `quantity` INTEGER NULL,
    `pu` INTEGER NOT NULL,
    `date_Cmd` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Message` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `message` VARCHAR(254) NOT NULL,
    `statue_mess` VARCHAR(100) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Avis` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `commentaire` VARCHAR(254) NOT NULL,
    `user_name` VARCHAR(50) NOT NULL,
    `date_avis` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Facture` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `montant` DOUBLE NOT NULL,
    `user_firstname` VARCHAR(50) NOT NULL,
    `date_facture` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `ligneCmd` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `montant` DOUBLE NOT NULL,
    `user_firstname` VARCHAR(50) NOT NULL,
    `payment` ENUM('ESPECE', 'PAYPAL', 'CB') NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
