/*
  Warnings:

  - You are about to drop the column `date_avis` on the `Avis` table. All the data in the column will be lost.
  - You are about to drop the column `user_name` on the `Avis` table. All the data in the column will be lost.
  - You are about to drop the column `date_Point` on the `Loyalty_point` table. All the data in the column will be lost.
  - You are about to drop the column `firstname` on the `Loyalty_point` table. All the data in the column will be lost.
  - You are about to drop the `Login` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Message` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Users` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `favory` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `ligneCmd` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[userId]` on the table `Adresse` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[menuId,userId]` on the table `Avis` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `userId` to the `Adresse` table without a default value. This is not possible if the table is not empty.
  - Added the required column `menuId` to the `Avis` table without a default value. This is not possible if the table is not empty.
  - Added the required column `userId` to the `Avis` table without a default value. This is not possible if the table is not empty.
  - Added the required column `orderId` to the `Facture` table without a default value. This is not possible if the table is not empty.
  - Added the required column `userId` to the `Loyalty_point` table without a default value. This is not possible if the table is not empty.
  - Added the required column `updatedAt` to the `Menu` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `Adresse` ADD COLUMN `userId` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `Avis` DROP COLUMN `date_avis`,
    DROP COLUMN `user_name`,
    ADD COLUMN `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    ADD COLUMN `menuId` INTEGER NOT NULL,
    ADD COLUMN `userId` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `Facture` ADD COLUMN `orderId` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `Loyalty_point` DROP COLUMN `date_Point`,
    DROP COLUMN `firstname`,
    ADD COLUMN `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    ADD COLUMN `userId` INTEGER NOT NULL;

-- AlterTable
ALTER TABLE `Menu` ADD COLUMN `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    ADD COLUMN `updatedAt` DATETIME(3) NOT NULL;

-- DropTable
DROP TABLE `Login`;

-- DropTable
DROP TABLE `Message`;

-- DropTable
DROP TABLE `Users`;

-- DropTable
DROP TABLE `favory`;

-- DropTable
DROP TABLE `ligneCmd`;

-- CreateTable
CREATE TABLE `User` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `uid` VARCHAR(100) NOT NULL,
    `firstname` VARCHAR(50) NULL,
    `lastname` VARCHAR(50) NULL,
    `tel` INTEGER NULL,
    `email` VARCHAR(191) NOT NULL,
    `email_token` VARCHAR(191) NULL,
    `password` VARCHAR(100) NOT NULL,
    `password_token` VARCHAR(255) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `is_email_verified` BOOLEAN NOT NULL DEFAULT false,
    `role` ENUM('USERS', 'ADMIN', 'CHEF') NULL,

    UNIQUE INDEX `users_uid_uindex`(`uid`),
    UNIQUE INDEX `User_email_key`(`email`),
    UNIQUE INDEX `User_email_token_key`(`email_token`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Favory` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `menuId` INTEGER NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    UNIQUE INDEX `Favory_menuId_userId_key`(`menuId`, `userId`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Messages` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `notificationId` INTEGER NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Notification` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `userId` INTEGER NOT NULL,
    `read` BOOLEAN NOT NULL DEFAULT false,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `LigneCmd` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `montant` DOUBLE NOT NULL,
    `user_firstname` VARCHAR(50) NOT NULL,
    `menuId` INTEGER NOT NULL,
    `orderId` INTEGER NOT NULL,
    `payment` ENUM('ESPECE', 'PAYPAL', 'CB') NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `Adresse_userId_key` ON `Adresse`(`userId`);

-- CreateIndex
CREATE UNIQUE INDEX `Avis_menuId_userId_key` ON `Avis`(`menuId`, `userId`);

-- AddForeignKey
ALTER TABLE `Adresse` ADD CONSTRAINT `Adresse_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Loyalty_point` ADD CONSTRAINT `Loyalty_point_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Favory` ADD CONSTRAINT `Favory_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Favory` ADD CONSTRAINT `Favory_menuId_fkey` FOREIGN KEY (`menuId`) REFERENCES `Menu`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Messages` ADD CONSTRAINT `Messages_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Messages` ADD CONSTRAINT `Messages_notificationId_fkey` FOREIGN KEY (`notificationId`) REFERENCES `Notification`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Notification` ADD CONSTRAINT `Notification_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Avis` ADD CONSTRAINT `Avis_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Avis` ADD CONSTRAINT `Avis_menuId_fkey` FOREIGN KEY (`menuId`) REFERENCES `Menu`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Facture` ADD CONSTRAINT `Facture_orderId_fkey` FOREIGN KEY (`orderId`) REFERENCES `Order`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `LigneCmd` ADD CONSTRAINT `LigneCmd_menuId_fkey` FOREIGN KEY (`menuId`) REFERENCES `Menu`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `LigneCmd` ADD CONSTRAINT `LigneCmd_orderId_fkey` FOREIGN KEY (`orderId`) REFERENCES `Order`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
