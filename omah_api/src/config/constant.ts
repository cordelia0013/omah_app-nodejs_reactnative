import dotenv from 'dotenv'




dotenv.config({
    path: `${__dirname}/.env`
})


export const PORT = process.env.PORT || '3009'
export const NODE_ENV =process.env.NODE_ENV
export const APP_BASE_URL = process.env.APP_BASE_URL || '/api/'


//exporter se fichiers dans les fichiers que si on a besoin d'utiliser une variable d'environnement