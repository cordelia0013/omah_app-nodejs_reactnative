
import * as argon2 from 'argon2';

export const hashPassword = async (password: string) => {
    const hashed = await argon2.hash(password);
    return hashed;
};

export const comparePassword = async (
    hashedPassword: string,
    password: string
) => {
    return await argon2.verify(hashedPassword, password);
};