import express from "express";
import cors from "cors";
import { mainRouter } from "../routes/router";
import { APP_BASE_URL } from "./constant";


export const createServer = () => {
    //on créer une fonction pour un serveur voir la doc
    const server = express();
    const app = express();
    server.use(express.json());
    mainRouter.use(function(_,res,next){
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-type, Accept"
        );
        next();
    });
    app.use(express.json());
    app.use(cors());
    
    server.use(APP_BASE_URL, mainRouter);
    server.use(
      cors({
        origin: "http://localhost:1234"
      })
    );
  
    return server;
  };