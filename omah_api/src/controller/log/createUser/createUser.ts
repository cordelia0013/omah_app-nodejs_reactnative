import { PrismaClient } from '@prisma/client';
import {  hashPassword } from '../../../config/utils/passwordUtils';
import { Request, Response } from 'express';

const prisma = new PrismaClient();

export const registerController = async (req: Request, res: Response) => {
    const firstname = req.body.firstName;
    const lastname = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;
    const hashedPassword = await hashPassword(password);

  try {
    const userPresent = await prisma.user.findFirst({
      where: {
        email: email,
      },
    });

    if (userPresent) {
      return res
        .status(409)
        .json({ message: 'User with email already exists' });
    }

    await prisma.user.create({
      data: {
        firstname,
        lastname,
        email,
        password:hashedPassword

      },
    });

    res.status(201).json({ message: 'User created' });
  } catch (error) {
    res.status(424).json({ message: 'Failed to create user' });
  }
};