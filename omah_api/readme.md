# Etape de creation du projet

## PARTIE BACKEND AVEC NODEJS ET PRISMA
### installation de node 

Utiliation du gestionnaire de paCquet npm

```bash
npm init

```

### mise en place de la structure du projet

nodemon: lance un processus nodejs et permet de whatcher (relancer) pour le  redemarer a chaque fois

- j'installe typescript: `npm install --save-dev typescript ts-node @types/node nodemon`

les librairies installées dans le `package.json`:

```json

  "devDependencies": {
    "@types/node": "^17.0.19",
    "nodemon": "^2.0.15",
    "ts-node": "^10.5.0",
    "typescript": "^4.5.5"
  }

```

Installation du fichier de configuration liée a typescript dans un fichier `tsconfig.json`

```
{
    "compilerOptions": {
      "target": "es6",
      "module": "commonjs",
      "lib": ["dom", "es6", "es2017", "esnext.asynciterable"],
      "skipLibCheck": true,
      "sourceMap": true,
      "outDir": "./dist",
      "moduleResolution": "node",
      "removeComments": false,
      "noImplicitAny": true,
      "strictNullChecks": true,
      "strictFunctionTypes": true,
      "noImplicitThis": true,
      "noUnusedLocals": true,
      "noUnusedParameters": true,
      "noImplicitReturns": true,
      "noFallthroughCasesInSwitch": true,
      "allowSyntheticDefaultImports": true,
      "esModuleInterop": true,
      "emitDecoratorMetadata": true,
      "experimentalDecorators": true,
      "resolveJsonModule": true,
      "baseUrl": "."
    },
    "exclude": ["node_modules"],
    "include": ["./src/**/*.ts"] // recupere tous les fichiers en ts pour la compilation en js
  }
```

#### dossier dist(build)

`npx tsc` : config pour compilé le code (build et dist)
si je souhaite executer le code d'un de mes fichiers en nodejs (index.ts par exemple); je fais:

`node ./dist/index.js`

#### ajouter des scripts 

```json
    "watch": "npx tsc -w", //recompile automatiquement quand le fichier change
    "build": "npx tsc",
    "dev": "nodemon ./dist/index.js", // lancer app et le watch
    "start": "node ./dist/index.js", //lance l'application
    "test": "npx jest"
```

### Express installation (creation de l'api)

express est un framework web.
il est légé et laisse le choix de choisir les elements que le souhaite.

Une ***api*** c'est faire des choses avec un protocole https selonsdes routes.
on va demander des resources en utilisant dans methodes(post, get, patch, put ...)
c'est un echange entre des requete des et response


1. installation

`npm install --save express` et `npm i --save-dev @types/express`

**ne pas oublier d'ajouter les @types après l'ajourt des librairies**: dans google tapez ` express github`, si dans le repo les @types ne sont pas notifié ou qu'il n y a  pas de typescript, il faudra récuperer les @types.

2. dans src, j'ajoute un fichier serveur.ts:

```js
export const createServer = async () => {
    //Initialization de notre server Express
    const server: express.Application = express();

    //Notre serveur parsera les requête entrante en Json
    server.use(express.json())

    return server
}
```
le fichier index.ts va permettre de determiner un port pour api

### creation des variable d'environnement (.env)

on creer un fichier .env

pour lire lesvariables dans un .env on ajoute une librairie appellé `dotenv`

` npm install --save-dev dotenv `

ajoute ensuite

```js
 import dotenv from 'dotenv'

dotenv.config()
```

en mono repo, il est important d ajouter le chemine pour arriver au .env

### mise en place du router de l'api


express contient un option de gestion du router.

ce fichier contiendra les differentes routes de l'api, pour rappel une route envoye une requete et recois une réponse.

je crée un fichier que j'appel `router.ts` à la racine du projet.

dans le .env je crée la route principale de l'api

```js
import { Router, Request, Response } from 'express' 
// dans la librairie express je recupere la methode router, 
//je recupere egalement 2 types d'express: request et response qui sont des interfaces



//creation du router

const mainRouter: Router = Router();

// creation de la 1ere route via un middleware

mainRouter.get('/', (_: Request, res: Response) => {
    res.json('Hello CPROM');
})

export default mainRouter

```

dans server.ts, qui est la configuration de notre serveur:

```js

// router.ts


import { Router, Request, Response } from 'express' 
// dans la librairie express je recupere la methode router, 
//je recupere egalement 2 types d'express: request et response qui sont des interfaces



//creation du router

const mainRouter: Router = Router();

// creation de la 1ere route via un middleware, c'est une route par defaut
// on ajoute un path et un handles ici un callback en params en on a une requete et une reponses

mainRouter.get('/', (req: Request, res: Response) => {
    res.json('Hello CPROM');
})

export default mainRouter

```

mise a jour du server.ts

```js

// server.ts

import express from 'express'
import mainRouter from './router'


import { APP_BASE_URL } from './constant';



export const createServer = () => {
    //Initialization de notre server Express
    const server =  express();

    //Notre serveur parsera les requêtes entrantes en Json
    server.use(express.json())


     //On rajoute le router à notre server afin qu'il puisse l'utiliser (En 1er params: url d'acces au router, 2eme params : nom du router) 

    server.use(APP_BASE_URL, mainRouter) 


    return server
}

```

quand on fait une requete (demande de ressource), on voyage avec un body (cor^de quelque chose et un header (cest les option et la configuration de la ressource que l'on souhaite)

Si on souhaite affichier les differente possibilité: 

```js

// router.ts
import { Router, Request, Response } from 'express' 



const mainRouter: Router = Router();


mainRouter.get('/', (req: Request, res: Response) => {
    console.log('Request :',req)
    console.log('Response :',res)
})

export default mainRouter

```
## PARTIE FRONTEND AVEC PARCEL

### Mise en places des pages login et register

### REUNIR LES ROUTES API A MON FRONTEND

1. j'install axiox : ` npm install --save-dev axios `

```js

import axios from 'axios'

...

//dans une fonction async le await parmet de de passer la ligne de code souhaiter en sync et de dire de l'action doit être fait obligatoirement avant les actions suivantes

await axios.get('http://localhost:3000/api/v1/')
```

