import { createServer } from './src/config/server';
import { PORT } from './src/config/constant'

import prisma from './databases/index'

const main = async () => {
const server = createServer();

const users = await prisma.user.findFirst();
console.log(users)
    server.listen(PORT, () => {
        
        console.log(`Server is now running on port ${process.env.PORT}`)
    })
}

main();