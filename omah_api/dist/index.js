"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./src/config/server");
const constant_1 = require("./src/config/constant");
const index_1 = __importDefault(require("./databases/index"));
const main = () => __awaiter(void 0, void 0, void 0, function* () {
    const server = (0, server_1.createServer)();
    const users = yield index_1.default.user.findFirst();
    console.log(users);
    server.listen(constant_1.PORT, () => {
        console.log(`Server is now running on port ${process.env.PORT}`);
    });
});
main();
//# sourceMappingURL=index.js.map