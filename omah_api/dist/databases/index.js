"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require("@prisma/client");
const dotenv_1 = __importDefault(require("dotenv"));
const path_1 = __importDefault(require("path"));
const envPath = path_1.default.join(__dirname, '../../');
dotenv_1.default.config({ path: envPath + '.env' });
const prisma = new client_1.PrismaClient({ log: ['query'] });
exports.default = prisma;
//# sourceMappingURL=index.js.map