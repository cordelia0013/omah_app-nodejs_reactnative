import React, { useEffect, useState } from "react";

//Route && Router
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { AuthRoute } from './js/pages/log/authRoute'



import { getLocalStorageItem } from "./js/utils/localStorage";

import "./css/app.css";
//Component
import Welcome from './js/pages/welcome/Welcome';




//screen
// import BoardAdmin from './js/pages/board/BoardAdmin';
// import BoardChef from './js/pages/board/BoardChef';
// import BoardUser from './js/pages/board/BoardUser';

// import MenuList from './js/pages/menuList/MenuList'

 import Profile from './js/pages/profile/Profile'
 import Login from './js/pages/log/connexion/Login';
  import Register from './js/pages/log/inscription/Register';
import { Layout } from "./js/pages/welcome/component/Layout";


export default App = () => {

   
    return (
        <Router>
        <Switch>
            {/* http://localhost:3008/register */}
            <Route path="/register" component={Register} />
            {/* http://localhost:3008/login */}
            <Route path="/login" component={Login} />
            <Layout>
                {/* http://localhost:3008/ */}
                <AuthRoute exact path="/" component={Welcome} />
                <AuthRoute path="/profile" component={Profile} />
            </Layout>
        </Switch>
    </Router>

  );
    
    }
    