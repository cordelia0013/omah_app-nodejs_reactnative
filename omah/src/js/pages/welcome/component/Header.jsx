import React, { Children, Component } from 'react'
import{BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom'

import ReactLogo from './../../../../../public/Asset/mixing-bowl.svg';
//import BurgerMenu from './burger/burgerMenu'


import '../../../../css/component.css';



export default class Headers extends Component {


  // constructor(props){
  //   super(props);
  //   //this.logout = this.logout.bind(this);

  //   this.state = {
  //     showModeratorBoard: false,
  //     showAdminBoard: false,
  //     currentUser: undefined
  //   };
  // }


  // componentDidMount(){
  //   const user = authService.getCurrentUser();

  //   if(user){
  //     this.setState({
  //       currentUser: authService.getCurrentUser(),
  //       showModeratorBoard: user.roles.includes ("role_ chef"),
  //       showAdminBoard: user.roles.includes("role_admin")
  //     })
  //   }
  // }

// logout(){
//   authService.logout();
// }

render() {

  const {currentUser, showAdminBoard, showChef} = this.state;
  return (

    <Router>
      <nav className="navbar navbar-expand-lg navbar-dark ">
    <div className="container-fluid">
      <Link to={"/"} className="navbar-brand" href="#"><img src={ReactLogo} alt="React Logo" />

      </Link>
      {showAdminBoard && (
            <li className="nav-item">
                <Link to={"/admin"} className="nav-link"> Admin board
                </Link>
            </li>
          )}


          {showChef && (
            <li className="nav-item">
                <Link to={"/chef"} className="nav-link"> Chef board
                </Link>
            </li>
          )}   

        {currentUser ? (
          <div className='navbar-nav ml-auto'>

            <li className="nav-item ">
                <Link to={"/profile"} className="nav-link"> {currentUser.lastname}
                </Link>
            </li>
               <li className="nav-item ">
               <Link to={"/login"} className="nav-link" onClick={this.logout}> <button className="btn-nav">Déconnexion</button>
               </Link>
           </li>

          </div>
        ):(
          <div className='navbar-nav ml-auto'>

            <li className="nav-item">
                <Link to={"/register"} className="nav-link"> <button className="btn-nav">Inscription</button>
                </Link>
            </li>
               <li className="nav-item ">
               <Link to={"/login"} className="nav-link" onClick={this.logout}> <button className="btn-nav">connexion</button>
               </Link>
           </li>

          </div>
        )}

    </div>
  </nav>
  </Router>
  )
}
}
