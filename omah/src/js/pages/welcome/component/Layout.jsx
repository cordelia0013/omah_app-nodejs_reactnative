import React from 'react'

import  Header  from './Header'


const Layout = ({ children }) => {
    return (
        <div>
            <Header />
            <div style={{ display: 'flex', flexGrow: '1' }}>
               
                <main style={{ backgroundColor: 'teal', width: '100%' }}>
                    {children}
                </main>
            </div>
        </div>
    )
}

export { Layout }
