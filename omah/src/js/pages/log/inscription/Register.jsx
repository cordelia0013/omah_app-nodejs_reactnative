import React, {useState, useEffect}from 'react'
import axios from "axios";

import classes from './register.module.css'
import {API_BASE_URL} from "../../../utils/constants";






export default function Register() {
  
  
  
  
        const initialValues = {
          firstName:"",
          lastName:"",
          email:"",
          password:"",
          UserRole:"default"
      };

  //gestion des states

  const [formValues, setFormValues] = useState(initialValues); 
  const [formErrors, setFormErrors] = useState({}); //GESTION DE ERREURS
  const [isSubmit, setIsSubmit ] = useState(false);

  const handleChange = (e) =>{
  //console.log(e.target)
  const {name, value} = e.target;
  setFormValues({...formValues,[name]:value});
  //console.log(formValues)
  }

  const handleSubmit =(e) =>{
    e.preventDefault();//eviter l'actualisation de la page
  setFormErrors(validateForm(formValues));
    setIsSubmit(true);

    axios.post(API_BASE_URL +`register`, { formValues })
          .then(res => {
             // console.log(res);
             // console.log(res.data);
              console.log("c'est enregistrer");
          })


  }


  useEffect( () =>{
   // console.log(formErrors);
    //permet de verifier si les 3 conditions du handleSubmit son ok
    if(Object.keys(formErrors).length === 0 && isSubmit){
    //  console.log(formValues);
    }
  },[formErrors])


  const validateForm =(values) => {
   
    const errors = {};

    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    
    if(!values.firstName){
      errors.firstName = "Champ requis"
    }

    if(!values.lastName){
      errors.lastName = "Champ requis"
    }

    if(!values.email){
      errors.email = "votre Email est requis"
    }else if(!regex.test(values.email)){
      errors.email = "ce n'est pas format d'email valide"
    }


    if(!values.password){
      errors.password = "votre password est requis"
    }else if(values.password.length < 4){
      errors.password = "le mot de passe doit avoir plus de 4 caractères"
    }else if(values.password.length > 10){
      errors.password = "le mot de passe ne doit excéder plus de 10 caractères"
    }
    return errors;
  };


    return (
      <>


      {/* {Object.keys(formErrors).length === 0 && isSubmit ? (
          <div className="ui message success">Signed in successfully</div>
        ) : (
          <pre>{JSON.stringify(formValues, undefined, 2)}</pre>
        )} */}

      <form onSubmit={handleSubmit}>

    <div>Login Form</div>

    <fieldset>
    <div className="form-group">

    <label htmlFor="InputfirstName" className="form-label mt-4"/>
      <input
          className="form-control" 
          id="Inputfirstname" 
          type="text"
          name="firstName"
          placeholder="firstname"
          value={formValues.firstName}
          onChange={handleChange}
          required
      />
    </div>
<p className='alert alert-danger'>{formErrors.firstName}</p>

{/* //////////// */}

  <div className="form-group">
    <label htmlFor="InputlastName" className="form-label mt-4"/>
        <input
            className="form-control" 
            id="InputlastName" 
            type="text"
            name="lastName"
            placeholder="lastName"
            value={formValues.lastName}
            onChange={handleChange}
            required
        />
  </div>   
<p className='alert alert-danger'>{formErrors.lastName}</p>

{/* //////////// */}

  <div className="form-group">
      <label htmlFor="InputEmail" className="form-label mt-4"/>
      <input
        className="form-control" 
        id="InputEmail" 
        type="text"
        name="email"
        placeholder="Email"
        value={formValues.email}
        onChange={handleChange}
        required
      />
  </div>
      <small id="emailHelp" className="form-text text-muted">Nous ne partagerons jamais votre e-mail avec qui que ce soit d'autre.</small>

<p className='alert alert-danger'>{formErrors.email}</p>

{/* //////////// */}
    <div className="form-group">
      <label htmlFor="InputPassword1" className="form-label mt-4"/>

      <input
       className="form-control" 
       id="InputPassword" 
              type="password"
              name="password"
              placeholder="Password"
              value={formValues.password}
              onChange={handleChange}
              required
            />
     
    </div>
    <p className='alert alert-danger'>{formErrors.password}</p>


    <br />
    <button type="submit" className="btn btn-primary btn-lg">Valider</button>

    </fieldset>
    </form>

    </>
  )
}
