import React, {useState, useEffect}from 'react'


import classes from './login.module.css'





export default function Login() {
  
  
  
  
        const initialValues = {
          email:"",
          password:""
      };

  //gestion des states

  const [formValues, setFormValues] = useState(initialValues); 
  const [formErrors, setFormErrors] = useState({}); //GESTION DE ERREURS
  const [isSubmit, setIsSubmit ] = useState(false);

  const handleChange = (e) =>{
  //console.log(e.target)
  const {name, value} = e.target;
  setFormValues({...formValues,[name]:value});
  //console.log(formValues)
  }

  const handleSubmit =(e) =>{
    e.preventDefault();//eviter l'actualisation de la page
  setFormErrors(validateForm(formValues));
    setIsSubmit(true);
  }


  useEffect( () =>{
    console.log(formErrors);
    //permet de verifier si les 3 conditions du handleSubmit son ok
    if(Object.keys(formErrors).length === 0 && isSubmit){
      console.log(formValues);
    }
  },[formErrors])


  const validateForm =(values) => {

    const errors = {};

    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    if(!values.email){
      errors.email = "votre Email est requis"
    }else if(!regex.test(values.email)){
      errors.email = "ce n'est pas format d'email valide"
    }


    if(!values.password){
      errors.password = "votre password est requis"
    }else if(values.password.length < 4){
      errors.password = "le mot de passe doit avoir plus de 4 caractères"
    }else if(values.password.length > 10){
      errors.password = "le mot de passe ne doit excéder plus de 10 caractères"
    }
    return errors;
  };


    return (
      <>


  

      {Object.keys(formErrors).length === 0 && isSubmit ? (
          <div className="ui message success">Signed in successfully</div>
        ) : (
          <pre>{JSON.stringify(formValues, undefined, 2)}</pre>
        )}

      <form onSubmit={handleSubmit}>

    <div>Login Form</div>

    <fieldset>
    <div className="form-group">

      <label htmlFor="InputEmail" className="form-label mt-4"/>
      <input
       className="form-control" 
       id="InputEmail" 
              type="text"
              name="email"
              placeholder="Email"
              value={formValues.email}
              onChange={handleChange}
              required
            />
  
      <small id="emailHelp" className="form-text text-muted">Nous ne partagerons jamais votre e-mail avec qui que ce soit d'autre.</small>
    </div>
<p className='alert alert-danger'>{formErrors.email}</p>

    <div className="form-group">
      <label htmlFor="InputPassword1" className="form-label mt-4"/>

      <input
       className="form-control" 
       id="InputPassword" 
              type="password"
              name="password"
              placeholder="Password"
              value={formValues.password}
              onChange={handleChange}
              required
            />
     
    </div>
    <p className='alert alert-danger'>{formErrors.password}</p>


    <br />
    <button type="submit" className="btn btn-primary btn-lg">Connexion</button>

    </fieldset>
    </form>

    </>
  )
}
