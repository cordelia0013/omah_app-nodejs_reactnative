import React, { Component } from 'react'
import classes from './Horloge.module.css'








export default class Horloge extends Component {

    // ajout de states

   state = {
       date: new Date(),
       compteur: 1
    };

 tick= () => {
     this.setState((oldState, props) => { 
         return{
            date: new Date(),
            compteur: oldState.compteur +1 // mettre a jour un state si elle dependant d'une valeur initiale
         }
       });
 }
   

    //le composant est monté (crée une premiere fois), on utilise le 
    //setState pour mettre a jour un componant. il rafraichit le componant
componentDidMount(){
   this.timerId = setInterval(() => this.tick(), 1000);
}

/// permet le demontage et suppression de component
componentWillUnmount(){
    clearInterval(this.timerId);
}

  render() {
    return (
        <>
      <h2 className={classes.monTitre}>Horloge: {this.state.date.toLocaleTimeString()}</h2>
      <div>Compteur: {this.state.compteur}</div>
      </>
    )
  }
}
