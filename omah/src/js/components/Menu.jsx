import React from 'react'

import '../../css/component.css';



import Card from './Card'



export default function Menu() {
  return (
    <>
      <ul className="nav nav-tabs navWidth">
  <li className="nav-item">
    <a className="nav-link active" data-bs-toggle="tab" href="#plat">Nos Plats</a>
  </li>

  <li className="nav-item">
    <a className="nav-link " data-bs-toggle="tab" href="#chef">Nos chefs</a>
  </li>


  <li className="nav-item">
    <a className="nav-link " data-bs-toggle="tab" href="#favory">Mes Favories</a>
  </li>

</ul>

<div id="myTabContent" className="tab-content">


  <div className="tab-pane fade active show" id="plat">
  <div  className='sectionCard'>
             <Card />
           <Card />
           <Card />  
           <Card />
           <Card />
           <Card /> 
             </div>
  </div>

  <div className="tab-pane fade " id="chef">
  <div   className='sectionCard'>
             <Card />
           <Card />
           <Card />  
           <Card />
           <Card />
           <Card /> 
             </div>
  </div>
  <div className="tab-pane fade " id="favory">
  <div   className='sectionCard'>
             <Card />
           <Card />
           <Card />  
           <Card />
           
             </div>
  </div>
 
</div>
    </>
  )
}
