import React from 'react'
import{BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom'


import '../../css/component.css';






export default function Header(props) {
  return (
    <Router>
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
    <div className="container-fluid">
      <Link to={"/"} className="navbar-brand" href="#">Omah</Link>
     
      <div className="collapse navbar-collapse" id="navbarColor01">
        <ul className="navbar-nav me-auto">
          
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{props.children}</a>
            <div className="dropdown-menu">
              <a className="dropdown-item" href="#">Point fidelité</a>
              <a className="dropdown-item" href="#">Mes commandes</a>
              <a className="dropdown-item" href="#">Mon profil</a>
              <div className="dropdown-divider"></div>
              <a className="dropdown-item" href="#">Déconnexion</a>
            </div>
          </li>
        </ul>
       
      </div>
    </div>
  </nav>
  </Router>
  )
}

































// export default function Header(props) {
//   return (
//     <>
//       <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
//     <div className="container-fluid">
//       <Link to={"/"} className="navbar-brand" href="#">Omah</Link>
//       <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
//         <span className="navbar-toggler-icon"></span>
//       </button>
  
//       <div className="collapse navbar-collapse" id="navbarColor01">
//         <ul className="navbar-nav me-auto">
          
//           <li className="nav-item dropdown">
//             <a className="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{props.children}</a>
//             <div className="dropdown-menu">
//               <a className="dropdown-item" href="#">Point fidelité</a>
//               <a className="dropdown-item" href="#">Mes commandes</a>
//               <a className="dropdown-item" href="#">Mon profil</a>
//               <div className="dropdown-divider"></div>
//               <a className="dropdown-item" href="#">Déconnexion</a>
//             </div>
//           </li>
//         </ul>
       
//       </div>
//     </div>
//   </nav>
//   </>
//   )
// }
